import React from "react"
import Helmet from "react-helmet"

/*
* Inject CSS class names here to fix some bulma issues on all pages using the navbar
*/ 

export default () => (
    <Helmet>
        <html className="has-navbar-fixed-top" /> 
    </Helmet>
)