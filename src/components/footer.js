import React from "react"
import { FaHeart, FaEnvelope, FaFacebook, FaInstagram } from "react-icons/fa"
import { Link } from "gatsby"

export default () => (
    <footer className="footer">
        <div className="container">
            <div className="columns">
                <div className="column">
                    <p style={{ marginBottom:"10px", fontWeight:"bold" }}>Unternehmen</p>
                    <ul >
                        <li><Link to="#">Team</Link></li>
                        <li><a href="https://motovio.de/blog">Blog</a></li>
                        <li><Link to="#">Presse</Link></li>
                    </ul>    
                </div>
                
                <div className="column">
                    <p style={{ marginBottom:"10px", fontWeight:"bold" }}>Produkt</p>
                    <ul >
                        <li><Link to="#">Die Plattform</Link></li>
                        <li><Link to="#">Deine Vorteile</Link></li>
                        <li><Link to="#">Kundenstimmen</Link></li>
                    </ul>
                </div>

                <div className="column">
                    <p style={{ marginBottom:"10px", fontWeight:"bold" }}>Rechtliches</p>
                    <ul >
                        <li><Link to="/impressum">Impressum</Link></li>
                        <li><a href="https://motovio.de/datenschutz">Datenschutz</a></li>
                    </ul>
                </div>

                <div className="column">
                    <p style={{ marginBottom:"10px", fontWeight:"bold" }}>Kontakt</p>
                    <ul>
                        <li><a href="mailto:hello@motovio.de"><span className="icon has-text-primary"><FaEnvelope/></span>Mail</a></li>
                        <li><a href="https://www.facebook.com/motovio/"><span className="icon color-facebook"><FaFacebook /></span>Facebook</a></li>
                        <li><a href="https://www.instagram.com/motovio.de/"><span className="icon color-instagram"><FaInstagram /></span>Instagram</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="has-text-centered" style={{ marginTop:'35px', marginBottom:"-45px"}}>
            Copyright 2018 - 2019 by motovio UG (haftungsbeschränkt)<br />
            Made with <span className="icon has-text-primary"><FaHeart /></span> in Mannheim
        </div>
    </footer>    
)