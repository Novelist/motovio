import { Link, navigate } from 'gatsby'
import { Link as SmoothLink, animateScroll as scroll } from 'react-scroll'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import { isLoggedIn, logout } from '../app/services/auth'
import Meta from "../components/meta"
import logo from "../images/motovio-logo.png"


class Header extends Component{

  state = {
    navbarBurger: "navbar-burger",
    navbarMenu: "navbar-menu"
  }

  toggleNavbar = () => {
    if(this.state.navbarBurger.includes("is-active")) {
        this.state = {
            navbarBurger: "navbar-burger",
            navbarMenu: "navbar-menu"
        }
        this.setState(this.state)
    } else {
        this.state = {
            navbarBurger: "navbar-burger is-active",
            navbarMenu: "navbar-menu is-active"
        }
        this.setState(this.state)
    }
  }

  render() {
    return (
      <div className="navbar is-fixed-top" role="navigation" aria-label="main navigation">    
        <Meta />
        <div className="container content is-medium">
          <div className="navbar-brand">
            {isLoggedIn() ? (
              <Link to="/app/overview" className="navbar-item">
                <img className="image" src={logo} alt="motovio-logo" draggable="false" />
              </Link>
            ) : (
              <Link to="/" className="navbar-item">
                <img className="image" src={logo} alt="motovio-logo" draggable="false" />
              </Link>
            )}

            <a role="button" className={this.state.navbarBurger} aria-label="menu" aria-expanded="false" onClick={this.toggleNavbar.bind(this)}>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>

      <div className={this.state.navbarMenu}>
        <div className="navbar-start"></div>

        {isLoggedIn() ? (
            <div className="navbar-end">
              <div className="navbar-item">
                <Link to="/app/overview">Kurse</Link>
              </div>

              <div className="navbar-item">
                <Link to="/app/profile">Profil</Link>
              </div>

              <div className="navbar-item">
                <div className="buttons">
                  <a className="button is-primary" href="/" onClick={event => {
                    event.preventDefault()
                    logout(() => navigate(`/`))
                  }}>
                    <strong>Logout</strong>
                  </a>
                </div>
              </div>
            </div>
          ) : (
            <div className="navbar-end">
              <div className="navbar-item">
                <SmoothLink to="platform" smooth={true} spy={true} offset={-150} duration={500}>Die Plattform</SmoothLink>
              </div>
              
              <div className="navbar-item">
                <SmoothLink to="benefits" smooth={true} spy={true} offset={-70} duration={500}>Deine Vorteile</SmoothLink>
              </div>
            
              <div className="navbar-item">
                <div className="buttons">
                  <Link to="/app/login/" className="button is-primary">
                    <strong>Jetzt starten</strong>
                  </Link>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
  </div>
    )
  }
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
