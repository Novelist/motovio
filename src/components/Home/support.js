import React from "react"
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

export const supportImage = graphql`
  fragment supportImage on File {
    childImageSharp {
      fluid(maxWidth: 500){
        ...GatsbyImageSharpFluid
      }
    }
  }
`

export default () => (
    <section className="section">
          <div className="container content has-text-centered">
            <h1>mit Unterstützung von</h1>
            <StaticQuery
                query={graphql`
                  query {
                    image1: file(relativePath: { eq: "home/support/vw.png" }) {
                      ...supportImage
                    }
                    image2: file(relativePath: { eq: "home/support/dc.png" }) {
                      ...supportImage
                    }
                    image3: file(relativePath: { eq: "home/support/cf.png" }) {
                      ...supportImage
                    }
                    image4: file(relativePath: { eq: "home/support/iw.png" }) {
                      ...supportImage
                    }
                    }
                  `} render={data => 
                    <div className="columns">
                      <div className="column">
                        <Img fluid={data.image1.childImageSharp.fluid} draggable="false" selectable="false" />
                      </div>

                      <div className="column">
                        <Img fluid={data.image2.childImageSharp.fluid} />
                      </div>

                      <div className="column">
                        <Img fluid={data.image3.childImageSharp.fluid} />
                      </div>

                      <div className="column">
                        <Img fluid={data.image4.childImageSharp.fluid} />
                      </div>
                    </div>
                  }/>
          </div>
    </section>
)