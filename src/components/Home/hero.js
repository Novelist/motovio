import React from "react"
import { Link, StaticQuery, graphql } from "gatsby"
import { Link as SmoothLink, animateScroll as scroll } from 'react-scroll'
import Img from 'gatsby-image'

export default () => (
        <section className="section motovioBg">
          <div className="container">
            <div className="columns is-vcentered">
             <div className="column">
                <div className="content is-large">
                  <h1>Save money,<br /> drive safe</h1>
                  <p>Teure KFZ-Versicherungen waren gestern.<br/> Mit motovio sicher fahren und Geld sparen.</p>
                  
                  <div className="buttons are-large">
                    <SmoothLink to="platform" smooth={true} spy={true} offset={-150} duration={500} className="button is-light">
                      <strong>Mehr erfahren</strong>
                    </SmoothLink>

                    <Link to="/app/login/" className="button is-primary">
                      <strong>Jetzt starten</strong>
                    </Link>
                  </div>
                </div>
              </div>
            
              <div className="column">
              <StaticQuery
                query={graphql`
                  query {
                    placeholderImage: file(relativePath: { eq: "home/hero.png" }) {
                      childImageSharp {
                        fluid(maxWidth: 800) {
                          ...GatsbyImageSharpFluid
                        }
                      }   
                    }
                  }
                `}
                render={data => <Img fluid={data.placeholderImage.childImageSharp.fluid} alt="motovio" draggable="false" unselectable="on" />}
                />    
              </div>
            </div> 
          </div>
        </section>
)