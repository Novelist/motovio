import React from "react"
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

export const squareImage = graphql`
  fragment squareImage on File {
    childImageSharp {
      fluid(maxWidth: 500, maxHeight: 500){
        ...GatsbyImageSharpFluid
      }
    }
  }
`


export default () => (
  
      <section className="section">
          <div className="container content has-text-centered">
            <h1>bekannt aus</h1>
            
              <StaticQuery
                query={graphql`
                  query {
                    image1: file(relativePath: { eq: "home/knownFrom/sb.png" }) {
                      ...squareImage
                    }
                    image2: file(relativePath: { eq: "home/knownFrom/sv.png" }) {
                      ...squareImage
                    }
                    image3: file(relativePath: { eq: "home/knownFrom/ac.png" }) {
                      ...squareImage
                    }
                    image4: file(relativePath: { eq: "home/knownFrom/tt.png" }) {
                      ...squareImage
                    }
                    }
                  `} render={data => 
                    <div className="columns">
                      <div className="column">
                        <Img fluid={data.image1.childImageSharp.fluid} alt="startbase-logo"/>
                      </div>

                      <div className="column">
                        <Img fluid={data.image2.childImageSharp.fluid} alt="startup-valley-logo" />
                      </div>

                      <div className="column">
                        <Img fluid={data.image3.childImageSharp.fluid} alt="asscompact-logo" />
                      </div>

                      <div className="column">
                        <Img fluid={data.image4.childImageSharp.fluid} alt="techtag-logo" />
                      </div>
                    </div>
                  }/>
          </div>
        </section>

)