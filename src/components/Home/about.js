import React from "react"
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

export const squareImage = graphql`
  fragment aboutImage on File {
    childImageSharp {
      fluid(maxWidth: 500, maxHeight: 500){
        ...GatsbyImageSharpFluid
      }
    }
  }

  fragment mockupImage on File {
    childImageSharp {
      fluid(maxWidth: 600, maxHeight: 550){
        ...GatsbyImageSharpFluid
      }
    }
  }
`

export default () => (
    <div className="section motovioBg" id="about">
          <StaticQuery
            query={graphql`
              query {
                drive: file(relativePath: { eq: "home/about/drive.png" }) {
                  ...aboutImage
                }
                learn: file(relativePath: { eq: "home/about/learn.png" }) {
                  ...aboutImage
                }
                save: file(relativePath: { eq: "home/about/save.png" }) {
                  ...aboutImage
                }
                mockup: file(relativePath: { eq: "home/mockup_about.png" }) {
                  ...mockupImage
                }
              }
            `} render={data => 
              <div className="container content is-medium" id="platform">
                <div className="columns" style={{ marginTop: "5rem"}} >
                  <div className="column">
                    <h1 className="has-text-primary">motovio ist dein KFZ-Assistent</h1>
                    <p><strong>Hohe KFZ-Versicherungsbeträge ärgern uns genauso wie dich.</strong></p>
                    <p>
                      Die Hälfte aller KFZ-Versicherungsnehmer würde Ihre Versicherung sofort wechseln. 
                      Der Grund: <strong>zu hohe Beiträge</strong>. Das muss nicht sein.
                    </p>
        
                    <p>
                      <ul>
                        <li>Erstelle ein kostenfreies Benutzerkonto auf unserer Plattform*.</li>
                        <li>Absolviere unsere Online-Lernkurse zu Themen der Verkehrssicherheit und Fahrzeugwartung.</li>
                        <li>Deine Versicherung gewährt dir Rabatte oder andere Boni.</li>
                      </ul>
                      <span style={{ fontSize: "12px" }}>* voraussichtlich verfügbar ab Mitte 2019</span>
                    </p>
                  </div>
              
                  <div className="column mockupWidth is-hidden-mobile">
                    <Img fluid={data.mockup.childImageSharp.fluid} alt="motovio-mockup" draggable="false" unselectable="true"/>
                  </div>
                </div>

                <div className="columns" style={{ marginTop: "5rem", marginBottom: "5rem"}} id="benefits">
                  <div className="column">
                    <div className="card is-usp">
                      <div className="card-image ">
                        <Img fluid={data.learn.childImageSharp.fluid} alt="motovio-mockup" draggable="false" unselectable="true"/>
                      </div>
                      <div className="card-content" style={{ marginTop: "-1.5rem" }}>
                        <p className="title">Mehr Wissen</p>
                        <div className="content">
                          Die regelmäßigen Kurse halten dein Wissen auf dem neusten Stand.
                        </div>
                      </div>
                    </div>
                  </div>
            
                  <div className="column">
                    <div className="card is-usp">
                      <div className="card-image ">
                        <Img fluid={data.drive.childImageSharp.fluid} alt="motovio-mockup" draggable="false" unselectable="true"/>
                      </div>
                      <div className="card-content" style={{ marginTop: "-1.5rem" }}>
                        <p className="title">Sicher fahren</p>
                        <div className="content">
                          Unsere Erinnerungen helfen dir, dein Auto in Schuss zu halten.
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="column">
                    <div className="card is-usp">
                      <div className="card-image ">
                        <Img fluid={data.save.childImageSharp.fluid} alt="motovio-mockup" draggable="false" unselectable="true"/>
                      </div>
                      <div className="card-content" style={{ marginTop: "-1.5rem" }}>
                        <p className="title">Geld sparen</p>
                        <div className="content">
                          Erhalte Rabatte oder Boni nach erfolgreichem Absolvieren der Kurse.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          }/>
    </div>
)