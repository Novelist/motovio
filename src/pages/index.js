import React from 'react'

import Layout from '../components/layout'
import Hero from "../components/Home/hero"
import Support from "../components/Home/support"
import About from "../components/Home/about"
import KnownFrom from "../components/Home/knownFrom"

import SEO from '../components/seo'


import { initAuth } from '../app/services/auth'

initAuth()

class IndexPage extends React.Component {
  state = { loading: false, msg: null }
  handleClick = e => {
    e.preventDefault()

    this.setState({ loading: true })
    fetch('/.netlify/functions/hello')
      .then(response => response.json())
      .then(json => this.setState({ loading: false, msg: json.msg }))
  }

  render() {
    const { loading, msg } = this.state
    return (
      <Layout>
        <SEO title="Home" keywords={[`versicherung`, `kfz-versicherung`, `autoversicherung`, `günstig`, `geld sparen`, `sparen`]} />
        <Hero />
        <Support />
        <About />
        <KnownFrom />
      </Layout>
    )
  }
}

export default IndexPage
