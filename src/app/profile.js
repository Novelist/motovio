import React from 'react'
import { getUser } from './services/auth'

const Profile = () => {
  const user = getUser()
  return (
    <>
      <section className="hero is-light is-bold">
          <div className="hero-body">
            <div className="container content is-large">
              <h1 className="title">Dein Profil</h1>
            </div>
          </div>
      </section>
      <section className="section">
        <div className="container content">
          <h1>Your profile</h1>
          <ul>
            <li>Name: {user.user_metadata && user.user_metadata.full_name}</li>
            <li>E-mail: {user.email}</li>
          </ul>
        </div>
      </section>
    </>
  )
}

export default Profile
