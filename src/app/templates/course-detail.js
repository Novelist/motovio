import React from "react"
import navigate from "gatsby"
import Layout from "../../components/layout"
import { isLoggedIn } from "../services/auth";

const CourseDetail = ({ pageContext: {node} }) => {
    
    return ( 
        <>
            <Layout>
            <section className="hero is-light is-bold">
                 <div className="hero-body">
                   <div className="container content">
                     <h1 className="title">{node.title}</h1> 
                 </div>        
                </div>
            </section>
        
            <section className="section">
                <div className="container content">
                    <div className="columns">
                        <div className="column is-one-third">
                            <div className="box">
                                <aside className="menu">
                                    <p className="menu-label">Lessons in this course</p>
                                    <ul className="menu-list" style={{ listStyle: "none", margin:"0", marginBottom:"15px"}}>
                                    {node.lessons.map(({id, title}) => (
                                        <li key={id}>
                                            <a>{title}</a>
                                        </li>
                                    ))}    
                                    </ul>
                                    <button className="button is-fullwidth is-success">Kurs starten</button>
                                </aside>
                            </div>
                        </div>

                        <div className="column">
                            <img src={node.image.file.url} alt={node.image.file.fileName} />
                            <p>
                                {node.description.description}
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    </>
    )
}

export default CourseDetail