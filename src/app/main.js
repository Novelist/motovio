import React from 'react'
import { StaticQuery, graphql, Link } from "gatsby"
import { getUser } from './services/auth'

class Main extends React.Component {
  state = { loading: false, json: null }
  handleClick = e => {
    e.preventDefault()
    const user = getUser()
    this.setState({ loading: true })
    fetch('/.netlify/functions/auth-hello', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + user.token.access_token,
      },
    })
      .then(response => response.json())
      .then(json => this.setState({ loading: false, json }))
  }

  render() {
    const { loading, json } = this.state
    const user = getUser()
    return (
      <>
        <section className="hero is-light is-bold">
          <div className="hero-body">
            <div className="container content is-large">
              <h1 className="title">Kursübersicht</h1>
            </div>
          </div>
        </section>

        <section className="section">
          <section className="container content">
            <div className="columns is-multiline">
              <StaticQuery
                query={graphql`
                  query {
                    allContentfulCourse {
                      edges {
                        node {
                          id
                          title
                          slug
                          shortDescription
                          image {
                            id
                            file {
                              url
                              fileName
                              contentType
                            }
                          }
                        }
                      }
                    }
                  }
                `} 
                render={data => (
                  data.allContentfulCourse.edges.map(({ node }) => (
                    <div className="column is-one-third" key={node.id}>
                      <Link to={"courses/" + node.slug}>
                        <div className="card is-course" key={node.id}>
                          <div className="card-image">
                            <figure className="image is-2by1">
                              <img src={node.image.file.url} alt={node.image.file.fileName} />
                            </figure>
                          </div>  
                          <div className="card-content">
                             <div className="content">
                                 <h3>{node.title}</h3>
                                 <p className="subtitle is-6">{node.shortDescription}</p>
                             </div>  
                          </div>
                        </div>
                      </Link>
                    </div>
                )))}/>
            </div>
          </section>
        </section>

        <section className="section">
          <div className="container content">
            <h1>Your Main App</h1>
            <ul>
              <li>API: {user.api && user.api.apiURL}</li>
              <li>ID: {user.id}</li>
            </ul>
            <hr />

            <button onClick={this.handleClick}>
              {loading ? 'Loading...' : 'Call Lambda Function'}
            </button>
            <pre>{JSON.stringify(json, null, 2)}</pre>    
          </div>          
        </section>
      </>
    )
  }
}

export default Main
