import React from 'react'
import { Router } from '@reach/router'
import { StaticQuery, graphql } from "gatsby"
import Layout from '../components/layout'
import Profile from './profile'
import Main from './main'
import CourseDetail from "./templates/course-detail"
import PrivateRoute from './components/PrivateRoute'
import Login from './login'


const App = () => {
  return (
    <Layout>
      <Router>
        <PrivateRoute path="/app/profile" component={Profile} />
        <PrivateRoute path="/app/overview" component={Main} />
        
        <PublicRoute path="/app">
          <Login path="/login" />
        </PublicRoute>
      </Router>
    </Layout>
  )
}
function PublicRoute(props) {
  return <div>{props.children}</div>
}

export default App
