import React from 'react'
import { navigate } from 'gatsby'
import { handleLogin } from './services/auth'

import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

export const squareImage = graphql`
  fragment heroImage on File {
    childImageSharp {
      fluid(maxWidth: 500){
        ...GatsbyImageSharpFluid
      }
    }
  }
`

class Login extends React.Component {
  handleSubmit = () => handleLogin(user => navigate(`/app/overview`))
  render() {
    return (
      <>
        <section className="section">
          <div className="container">
            <div className="columns is-vcentered">
            <StaticQuery
                query={graphql`
                  query {
                    login: file(relativePath: { eq: "home/hero_login.png" }) {
                      ...heroImage
                    }
                    
                  }
                  `} render={data => 
                    <div className="column">
                      <Img fluid={data.login.childImageSharp.fluid} alt="hero-login" draggable="false" unselectable="on" />    
                    </div>
                  }/>
              
      
             <div className="column">
                <div className="content is-medium">
                  <h1 className="has-text-centered">Save money, drive safe</h1>
                  <p>Besuche Online-Kurse über Verkerssicherheit und die korrekte Wartung deines Fahrzeugs. Absolvierst du die Kurse erfolgreich, winken dir attraktive Boni unserer Partner-Versicherungen</p>
              
                  <div className="box">
                <div className="content is-medium has-text-centered">
                  <p>Melde dich jetzt kostenlos an uns starte mit den ersten Kursen.</p>
                  <div className="columns">
                    <div className="column">
                      <button className="button is-light is-fullwidth" onClick={this.handleSubmit}>Registrieren</button>      
                    </div>
                    
                    <div className="column">
                      <button className="button is-primary is-fullwidth" onClick={this.handleSubmit}>Einloggen</button>      
                    </div>
                  </div>
                </div>
              </div>
                </div>
              </div>
            </div> 
          </div>
        </section>
      </>
    )
  }
}

export default Login
