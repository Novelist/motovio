/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
exports.createPages = async ({ actions, graphql }) => {
    //get all data relevant for creating course overview and course detail
    const { data } = await graphql(`
        query {
            allContentfulCourse {
              totalCount
              edges {
                node {
                  id
                  title
                  slug
                  shortDescription
                  description {
                    description
                  }
                  lessons {
                    id
                    title
                  }
                  image {
                    id
                    file {
                        url
                        fileName
                        contentType
                    }
                  }
                }
              }
            }
        }
    `)

    data.allContentfulCourse.edges.forEach(({ node }) => {
        //var slug = node.slug
        actions.createPage({
            path: "courses/" + node.slug,
            component: require.resolve("./src/app/templates/course-detail.js"),
            context: {
                node   
            }
        })
    }) 
}